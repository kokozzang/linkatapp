angular.module('linkat.comments.controllers', [])
  .controller('CommentListCtrl',
  ['$scope', '$log', '$ionicHistory', '$stateParams', '$ionicScrollDelegate', 'Session', 'Comments',
    function($scope, $log, $ionicHistory, $stateParams, $ionicScrollDelegate, Session, Comments) {
      $scope.comment = {};

      $log.debug('$ionicHistory.viewHistory()', $ionicHistory.viewHistory());

      Comments.all($stateParams.postId).then(function (success){
        $log.debug('all', success);
        $scope.comments = success.comments;
        $ionicScrollDelegate.scrollBottom({shouldAnimate: true});
      }, function (error){
        $log.debug('에러', error);
      });

      $scope.insert = function (){
        var postId = $stateParams.postId;//글 아이디
        $scope.comment.ownerId = $stateParams.ownerId;//글 소유자 아이디

        $log.debug('insert $scope.comment', $scope.comment);

        Comments.insert(postId, $scope.comment).then(function (success){
          $log.debug('insert', success);
          var comment = {
            commentId: success.commentId,
            user_url: Session.get('userUrl'),
            profile_small_url: Session.get('profileSmall'),
            nickname: Session.get('nickname'),
            content: $scope.comment.content,
            created_date: Date.now()
          };
          $scope.comment.content = '';
          $scope.comments.push(comment); //댓글 추가
          $ionicScrollDelegate.scrollBottom({shouldAnimate: true});
        }, function (error){
          $log.debug('에러', error);
        });
      };
    }
  ])
  .controller('CommentCtrl',
  ['$scope', '$log', '$stateParams', '$mdDialog', 'AuthService', 'Comments',
    function($scope, $log, $stateParams, $mdDialog, AuthService, Comments) {
      var comment = null;
      var $parentScope = $scope.$parent;

      //롱 터치시에 나타나는 모달
      $scope.openMenu = function (ev){
        comment = $scope.comment;
        $mdDialog.show({
          controller: MenuDialogCtrl,
          templateUrl: 'templates/comment/comment-modal.html',
          targetEvent: ev,
          locals: {
            isMine: AuthService.isMine(comment.user_id)
          }
        });
      };
      function MenuDialogCtrl($scope, $mdDialog, isMine){
        $scope.isMine = isMine;
        //취소
        $scope.cancel = function (ev){
          $mdDialog.cancel();
        };

        //삭제
        $scope.delete = function (ev){
          $mdDialog.hide();

          var confirm = $mdDialog.confirm()
            .content('댓글을 삭제하시겠어요?')
            .ariaLabel('confirm')
            .ok('삭제')
            .cancel('취소');

          //댓글 삭제 확인 다이얼로그
          $mdDialog.show(confirm).then(function() {
            var postId = $stateParams.postId;//글 아이디
            var commentId = comment.id;

            Comments.delete(postId, commentId).then(function (success){
              $log.debug('delete', success);
              //dom에서 댓글 제거: parent 컨트롤러의 댓글배열 ng-repeat
              _.remove($parentScope.comments, function (arr){
                if(arr.id === commentId)
                  return true;
                return false;
              });
            }, function (error){
              $log.debug('delete error', error);
            });
          }, function() {
            $log.debug('취소');
          });
        };//delete
      }//MenuDialogCtrl

    }//function
  ]);
