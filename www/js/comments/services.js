angular.module('linkat.comments.services', [])
    .factory('Comments',
    ['$http', '$log', '$q', 'Session', 'Env',
        function($http, $log, $q, Session, Env) {
            var accessToken = Session.get('accessToken');

            return {
                all: function (postId) {
                    var config = {
                        method: 'GET',
                        url: Env.API_URL + '/posts/' + postId + '/comments',
                        headers: {
                            'x-auth-token': accessToken
                        }
                    };
                    var deferred = $q.defer();
                    $http(config)
                        .then(function (result, status, headers, config) {
                            deferred.resolve(result.data);
                        }, function (msg) {
                            deferred.reject(msg);
                        });
                    return deferred.promise;
                },
                insert: function (postId, comment) {
                    var config = {
                        method: 'POST',
                        url: Env.API_URL + '/posts/' + postId + '/comments',
                        headers: {
                            'x-auth-token': accessToken
                        },
                        data: comment
                    };

                    var deferred = $q.defer();
                    $http(config)
                        .then(function (result, status, headers, config) {
                            deferred.resolve(result.data);
                        }, function (msg) {
                            deferred.reject(msg);
                        });
                    return deferred.promise;
                },
                delete: function (postId, commentId) {
                    var config = {
                        method: 'DELETE',
                        url: Env.API_URL + '/posts/' + postId + '/comments/' + commentId,
                        headers: {
                            'x-auth-token': accessToken
                        }
                    };

                    var deferred = $q.defer();
                    $http(config)
                        .then(function (result, status, headers, config) {
                            deferred.resolve(result.data);
                        }, function (msg) {
                            deferred.reject(msg);
                        });
                    return deferred.promise;
                }
            };//return
        }
    ]);