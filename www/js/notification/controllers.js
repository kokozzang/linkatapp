angular.module('linkat.notification.controllers', [])
  .controller('NotificationListCtrl',
  ['$scope', '$log', 'Notifications', 'ENUMS',
    function($scope, $log, Notifications, ENUMS) {
      $scope.EVENT = ENUMS.EVENT;

      Notifications.all().then(function (success){
        $log.debug('all', success);
        $scope.notifications = success.notifications;
      }, function (error){
        $log.debug('에러', error);
      });

    }
  ])

  .controller('NotificationCtrl',
  ['$scope', '$log', '$stateParams', '$mdDialog', 'AuthService', 'Notifications',
    function($scope, $log, $stateParams, $mdDialog, AuthService, Notifications) {



    }//function
  ]);
