angular.module('linkat.follow.controllers', [])
  .controller('followerListCtrl',
  ['$scope', '$log', '$stateParams', 'Session', 'Follow',
    function($scope, $log, $stateParams, Session, Follow) {
      Follow.followerList($stateParams.userUrl).then(function (success){
        $log.debug('all', success);
        $scope.followers = success.followers;
      }, function (error){
        $log.debug('에러', error);
      });

      //팔로우 토글
      $scope.toggleFollow = function (){
        $log.debug('follow $scope.follow', $scope.follower);
        if(_.isNull($scope.follower.followed)){
          $scope.follower.followed = true;
          Follow.insert($scope.follower.id).then(function (success){
            $log.debug('insert', success);
          }, function (error){
            $log.debug('에러', error);
          });
        }else{
          $scope.follower.followed = null;
          Follow.delete($scope.follower.id).then(function (success){
            $log.debug('delete', success);
          }, function (error){
            $log.debug('에러', error);
          });
        }
      };
    }
  ])

  .controller('followingListCtrl',
  ['$scope', '$log', '$stateParams', 'Session', 'Follow',
    function($scope, $log, $stateParams, Session, Follow) {
      Follow.followingList($stateParams.userUrl).then(function (success){
        $log.debug('all', success);
        $scope.followings = success.followings;
      }, function (error){
        $log.debug('에러', error);
      });

      //팔로우 토글
      $scope.toggleFollow = function (){
        $log.debug('follow $scope.follow', $scope.following);
        if(_.isNull($scope.following.followed)){
          $scope.following.followed = true;
          Follow.insert($scope.following.id).then(function (success){
            $log.debug('insert', success);
          }, function (error){
            $log.debug('에러', error);
          });
        }else{
          $scope.following.followed = null;
          Follow.delete($scope.following.id).then(function (success){
            $log.debug('insert', success);
          }, function (error){
            $log.debug('에러', error);
          });
        }
      };
    }
  ]);