angular.module('linkat.constants', [])
  .constant('angularMomentConfig', {timezone: 'Asia/Seoul'})
  .constant('GCM', {SENDER: '603834491680'})
  .constant('ENUMS', {
    MODE: {
      DEBUG: true,
      RELEASE: false
    },
    OAUTH: {
      LOCAL: 0,
      FACEBOOK: 1,
      TWITTER: 2
    },
    USER_STATE: {
      SUSPENDED: -2,   //계정정지
      DEACTIVATED: -1, //비활성화
      PENDING: 0,      //회원정보미입력
      UNCERTIFIED: 1,  //미인증
      ACTIVE: 2        //활성
    },
    CATEGORY: {
      CLOTHES: 1, //의류
      SHOES: 2,  //신발
      ETCS: 3 //잡화
    },
    NEWSFEED: {
      NEW: 0,
      HOT: 1
    },
    EVENT: {
      COMMENT: 1,       //댓글
      QUESTION_LIKE: 2, //나도 궁금
      ANSWER: 3,        //답변
      ANSWER_LIKE: 4   //갖고싶어
    },
    PAGE_LOAD_LIMIT: 20
  })
  .constant('EVT_PROFILE_UPDATE', {
    NICKNAME: 'PROFILE_NICKNAME_UPDATED',
    PROFILE_PHOTO: 'PROFILE_PHOTO_UPDATED'
  })

  .constant('OAUTH', {
    FACEBOOK: {
      APP_ID: '552515698191935'
    }
    //},
    //TWITTER: {
    //  CONSUMER_KEY: "YgR6hNIEtmcgdhoCRPjTpzQq4",
    //  CONSUMER_SECRET: "5pBoYoBa3N17KnEYCPhMuPJhhLOkfkuWuKknqSwGXTAn61YUkJ",
    //  CALLBACK_URL: "http://www.chopster.co.kr:3000/auth/twitter/callback"
    //}
  })

  .constant('ENV', {
    RELEASE: {
      API_URL: 'https://www.chopster.co.kr',
      LOG: false
    },
    DEBUG: {
      API_URL: 'https://www.chopster.co.kr',
      LOG: true
    }
  })

  .constant('USER_ROLES', {
    all: '*',
    admin: 'admin',
    editor: 'editor',
    guest: 'guest'
  })
  .constant('AUTH_EVENTS', {
    loginSuccess: 'auth-login-success',
    loginFailed: 'auth-login-failed',
    logoutSuccess: 'auth-logout-success',
    sessionTimeout: 'auth-session-timeout',
    notAuthenticated: 'auth-not-authenticated',
    notAuthorized: 'auth-not-authorized'
  })

  .constant('$ionicLoadingConfig', {
    noBackdrop: true,
    template: '<i class="ion-loading-c" style="color: #666; font-size: 30px"></i>'
  })
  .constant('_', _);  //lodash