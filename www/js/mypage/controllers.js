angular.module('linkat.mypage.controllers', [])
  .controller('MyPageCtrl',
  ['$scope', '$log', '$state', '$ionicHistory', '$stateParams', 'Mypage', 'Follow', 'AuthService',
    function($scope, $log, $state, $ionicHistory, $stateParams, Mypage, Follow, AuthService) {
      $log.debug('mypage $stateParams', $stateParams);
      $scope.user = {nickname: null};

      Mypage.profile($stateParams.userUrl).then(function (result){
        $scope.user = result.user;
        $scope.mypageBg = {'background': 'linear-gradient( rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6) ), url('+ $scope.user.profile_large_url + ')'};
        $log.debug('$scope.user', $scope.user);
        $scope.user.isMine = AuthService.isMine($scope.user.id);
      });
      
      $scope.data = {selectedIndex : 0};

      $scope.profileSetting = function (){
        $state.go('app.settings-profile');
      };

      $scope.toggleFollow = function (){
        $log.debug('follow $scope.user', $scope.user);
        if(_.isNull($scope.user.followed)){
          $scope.user.followed = true;
          $scope.user.follower_count += 1;
          Follow.insert($scope.user.id).then(function (success){
            $log.debug('insert', success);
          }, function (error){
            $log.debug('에러', error);
          });
        }else{
          $scope.user.followed = null;
          $scope.user.follower_count -= 1;
          Follow.delete($scope.user.id).then(function (success){
            $log.debug('delete', success);
          }, function (error){
            $log.debug('에러', error);
          });
        }
      };

      $scope.followers = function (){
        $state.go('app.followers', {userUrl: $scope.user.user_url});
      };

      $scope.followings = function (){
        $state.go('app.followings', {userUrl: $scope.user.user_url});
      };

      $scope.tabQlikes = function (){
        $state.go('app.mypage.qlikes', {userUrl: $scope.user.user_url});
      };
      $scope.tabAlikes = function (){
        $state.go('app.mypage.alikes', {userUrl: $scope.user.user_url});
      };
      $scope.tabQuestions = function (){
        $state.go('app.mypage.questions', {userUrl: $scope.user.user_url});
      };
      $scope.tabAnswers = function (){
        $state.go('app.mypage.answers', {userUrl: $scope.user.user_url});
      };


      ////무한스크롤
      //$scope.loadPosts = function (){
      //
      //  Categories.list(category, startId, page).then(function (success) {
      //    $log.debug('success.posts', success.posts);
      //    $scope.posts = $scope.posts.concat(success.posts);
      //
      //    //로드한 post 갯수가 limit보다 작은 경우
      //    if(success.posts.length < ENUMS.PAGE_LOAD_LIMIT)
      //      $scope.moreDataCanBeLoaded = false;
      //    page++;
      //    $scope.$broadcast('scroll.infiniteScrollComplete');
      //  }, function (error){
      //    $log.debug('error', error);
      //  });
      //};

    }
  ])

  .controller('MyPageTabQlikesCtrl',
  ['$scope', '$log', '$stateParams', 'Mypage', 'ENUMS',
    function($scope, $log, $stateParams, Mypage, ENUMS) {
      $scope.startId = null; //첫번째 postId
      $scope.page = 1; //현재 페이지

      $log.debug('MyPageTabQlikesCtrl $stateParams', $stateParams);
      Mypage.qlikes($stateParams.userUrl, $scope.startId, $scope.page).then(function (success){
        $scope.posts = success.questions;

        $scope.moreDataCanBeLoaded = true;  //무한스크롤 가능여부
        $scope.startId = $scope.posts.length ? $scope.posts[0].id: null;

        //로드한 post 갯수가 limit보다 작은 경우
        if(success.questions.length < ENUMS.PAGE_LOAD_LIMIT)
          $scope.moreDataCanBeLoaded = false;
        $scope.page++;
      });


      //refresh
      $scope.$parent.refresh = function (){
        $scope.startId = null;
        $scope.page = 1;
        $scope.moreDataCanBeLoaded = true;

        Mypage.qlikes($stateParams.userUrl, $scope.startId, $scope.page).then(function (success){
          $scope.posts = success.questions;
          //로드한 post 갯수가 limit보다 작은 경우
          if(success.questions.length < ENUMS.PAGE_LOAD_LIMIT)
            $scope.moreDataCanBeLoaded = false;
          $scope.page++;
          $scope.$parent.$broadcast('scroll.refreshComplete');
        }, function (err){
          $log.debug('err', err);
        });
      };

      //무한스크롤
      $scope.loadPosts = function (){
        Mypage.qlikes($stateParams.userUrl, $scope.startId, $scope.page).then(function (success) {
          $log.debug('success.questions', success.questions);
          $scope.posts = $scope.posts.concat(success.questions);

          //로드한 post 갯수가 limit보다 작은 경우
          if(success.questions.length < ENUMS.PAGE_LOAD_LIMIT)
            $scope.moreDataCanBeLoaded = false;
          $scope.page++;
          $scope.$parent.$broadcast('scroll.infiniteScrollComplete');
        }, function (error){
          $log.debug('error', error);
        });
      };

    }
  ])

  .controller('MyPageTabAlikesCtrl',
  ['$scope', '$log', '$stateParams', 'Mypage', 'ENUMS',
    function($scope, $log, $stateParams, Mypage, ENUMS) {
      $scope.startId = null; //첫번째 postId
      $scope.page = 1; //현재 페이지

      Mypage.alikes($stateParams.userUrl, $scope.startId, $scope.page).then(function (success){
        $scope.answers = success.answers;

        $scope.moreDataCanBeLoaded = true;  //무한스크롤 가능여부
        $scope.startId = $scope.answers.length > 0 ? $scope.answers[0].id: null;

        //로드한 post 갯수가 limit보다 작은 경우
        if(success.answers.length < ENUMS.PAGE_LOAD_LIMIT)
          $scope.moreDataCanBeLoaded = false;
        $scope.page++;
      });

      //refresh
      $scope.$parent.refresh = function (){
        $scope.startId = null;
        $scope.page = 1;
        $scope.moreDataCanBeLoaded = true;

        Mypage.alikes($stateParams.userUrl, $scope.startId, $scope.page).then(function (success){
          $scope.answers = success.answers;
          //로드한 post 갯수가 limit보다 작은 경우
          if(success.answers.length < ENUMS.PAGE_LOAD_LIMIT)
            $scope.moreDataCanBeLoaded = false;
          $scope.page++;
          $scope.$parent.$broadcast('scroll.refreshComplete');
        }, function (err){
          $log.debug('err', err);
        });
      };

      //무한스크롤
      $scope.loadPosts = function (){
        Mypage.alikes($stateParams.userUrl, $scope.startId, $scope.page).then(function (success) {
          $log.debug('success.answers', success.answers);
          $scope.answers = $scope.answers.concat(success.answers);

          //로드한 post 갯수가 limit보다 작은 경우
          if(success.answers.length < ENUMS.PAGE_LOAD_LIMIT)
            $scope.moreDataCanBeLoaded = false;
          $scope.page++;
          $scope.$parent.$broadcast('scroll.infiniteScrollComplete');
        }, function (error){
          $log.debug('error', error);
        });
      };
    }
  ])

  .controller('MyPageTabQuestionsCtrl',
  ['$scope', '$log', '$stateParams', 'Mypage', 'ENUMS',
    function($scope, $log, $stateParams, Mypage, ENUMS) {
      $scope.startId = null; //첫번째 postId
      $scope.page = 1; //현재 페이지

      $log.debug('MyPageTabQuestionsCtrl $stateParams', $stateParams);
      Mypage.questions($stateParams.userUrl, $scope.startId, $scope.page).then(function (success){
        $scope.posts = success.questions;

        $scope.moreDataCanBeLoaded = true;  //무한스크롤 가능여부
        $scope.startId = $scope.posts.length ? $scope.posts[0].id: null;

        //로드한 post 갯수가 limit보다 작은 경우
        if(success.questions.length < ENUMS.PAGE_LOAD_LIMIT)
          $scope.moreDataCanBeLoaded = false;
        $scope.page++;
      });

      //refresh
      $scope.$parent.refresh = function (){
        $scope.startId = null;
        $scope.page = 1;
        $scope.moreDataCanBeLoaded = true;

        Mypage.questions($stateParams.userUrl, $scope.startId, $scope.page).then(function (success){
          $scope.posts = success.questions;
          //로드한 post 갯수가 limit보다 작은 경우
          if(success.questions.length < ENUMS.PAGE_LOAD_LIMIT)
            $scope.moreDataCanBeLoaded = false;
          $scope.page++;
          $scope.$parent.$broadcast('scroll.refreshComplete');
        }, function (err){
          $log.debug('err', err);
        });
      };

      //무한스크롤
      $scope.loadPosts = function (){
        Mypage.questions($stateParams.userUrl, $scope.startId, $scope.page).then(function (success) {
          $log.debug('success.questions', success.questions);
          $scope.posts = $scope.posts.concat(success.questions);

          //로드한 post 갯수가 limit보다 작은 경우
          if(success.questions.length < ENUMS.PAGE_LOAD_LIMIT)
            $scope.moreDataCanBeLoaded = false;
          $scope.page++;
          $scope.$parent.$broadcast('scroll.infiniteScrollComplete');
        }, function (error){
          $log.debug('error', error);
        });
      };
    }
  ])

  .controller('MyPageTabAnswersCtrl',
  ['$scope', '$log', '$stateParams', 'Mypage', 'ENUMS',
    function($scope, $log, $stateParams, Mypage, ENUMS) {
      $scope.startId = null; //첫번째 postId
      $scope.page = 1; //현재 페이지

      $log.debug('MyPageTabAnswersCtrl $stateParams', $stateParams);
      Mypage.answers($stateParams.userUrl, $scope.startId, $scope.page).then(function (success){
        $scope.answers = success.answers;

        $scope.moreDataCanBeLoaded = true;  //무한스크롤 가능여부
        $scope.startId = $scope.answers.length > 0 ? $scope.answers[0].id: null;

        //로드한 post 갯수가 limit보다 작은 경우
        if(success.answers.length < ENUMS.PAGE_LOAD_LIMIT)
          $scope.moreDataCanBeLoaded = false;
        $scope.page++;
      });


      //refresh
      $scope.$parent.refresh = function (){
        $scope.startId = null;
        $scope.page = 1;
        $scope.moreDataCanBeLoaded = true;

        Mypage.answers($stateParams.userUrl, $scope.startId, $scope.page).then(function (success){
          $scope.answers = success.answers;
          //로드한 post 갯수가 limit보다 작은 경우
          if(success.answers.length < ENUMS.PAGE_LOAD_LIMIT)
            $scope.moreDataCanBeLoaded = false;
          $scope.page++;
          $scope.$parent.$broadcast('scroll.refreshComplete');
        }, function (err){
          $log.debug('err', err);
        });
      };

      //무한스크롤
      $scope.loadPosts = function (){
        Mypage.answers($stateParams.userUrl, $scope.startId, $scope.page).then(function (success) {
          $log.debug('success.answers', success.answers);
          $scope.answers = $scope.answers.concat(success.answers);

          //로드한 post 갯수가 limit보다 작은 경우
          if(success.answers.length < ENUMS.PAGE_LOAD_LIMIT)
            $scope.moreDataCanBeLoaded = false;
          $scope.page++;
          $scope.$parent.$broadcast('scroll.infiniteScrollComplete');
        }, function (error){
          $log.debug('error', error);
        });
      };
    }
  ]);