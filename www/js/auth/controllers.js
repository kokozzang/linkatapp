angular.module('linkat.auth.controllers', [])
  .controller('IntroCtrl',
  ['$rootScope', '$scope', '$state', '$localstorage', '$log', '$ionicHistory', '$cordovaOauth', 'AuthService', 'Session', 'ENUMS','OAUTH',
    function($rootScope, $scope, $state, $localstorage, $log, $ionicHistory, $cordovaOauth, AuthService, Session, ENUMS, OAUTH) {
      $scope.images = ['http://lorempixel.com/g/300/500/?1',
        'http://lorempixel.com/g/300/500/?2',
        'http://lorempixel.com/g/300/500/?3',
        'http://lorempixel.com/g/300/500/?4',
        'http://lorempixel.com/g/300/500/?5'];

      $scope.user = {};

      //로컬스토리지 보기
      $scope.getLocalStorage = function() {
        $log.debug('$localStorage.getAll()', $localstorage.getAll());
      };
      //로컬스토리지 날리기
      $scope.rmLocalStorage = function() {
        $localstorage.removeAll();
      };


      $scope.facebookLogin = function() {
        $cordovaOauth.facebook(OAUTH.FACEBOOK.APP_ID, ['public_profile, email'])
          .then(function(success) {
            $log.debug('success', success.access_token);

            //리턴 값
            //success.authResponse = {
            //  access_token, expires_in
            //}
            var facebook = {
              accessToken: success.access_token,  //액세스토큰
              date: new Date()  //날짜
            };

            AuthService.signIn(ENUMS.OAUTH.FACEBOOK, facebook.accessToken)
              .then(function (success){
                $localstorage.setObject('facebook', facebook);  //로컬스토리지에 저장
                var userState = Session.get('state');
                $log.debug('Session.get(state)', userState);

                if (_.isEqual(userState, ENUMS.USER_STATE.PENDING)) {
                  $log.debug('USER_STATE.PENDING');
                  $rootScope.$broadcast('EVT_REGISTER_GCM', null);  //로그인함
                  $ionicHistory.currentView($ionicHistory.backView());
                  $state.go('app.registerInfo');
                }
                else if (_.isEqual(userState, ENUMS.USER_STATE.UNCERTIFIED)) {
                  $log.debug('USER_STATE.UNCERTIFIED');
                  $rootScope.$broadcast('EVT_REGISTER_GCM', null);  //로그인함
                  $ionicHistory.currentView($ionicHistory.backView());
                  $state.go('app.newsfeed', {feed: ENUMS.NEWSFEED.HOT}, {location: 'replace'});
                }
                else if (_.isEqual(userState, ENUMS.USER_STATE.ACTIVE)) {
                  $log.debug('USER_STATE.ACTIVE');
                  $rootScope.$broadcast('EVT_REGISTER_GCM', null);  //로그인함
                  $ionicHistory.currentView($ionicHistory.backView());
                  $state.go('app.newsfeed', {feed: ENUMS.NEWSFEED.HOT}, {location: 'replace'});
                }else {
                  $state.go('app.intro');
                }
              }, function (error){
                $log.debug('signIn error', error);
              });

          }, function (error) {
            $log.debug('error', error);
          });
      }





    /*
    페이스북 Native Login
    */
    //$scope.facebookLogin = function() {
    //  $cordovaFacebook.login(['public_profile'])
    //    .then(function(success) {
    //      $log.debug('success', success.authResponse);
    //
    //      //리턴 값
    //      //success.authResponse = {
    //      //  accessToken, expiresIn, session_key, sig, userID
    //      //}
    //      var facebook = {
    //        accessToken: success.authResponse.accessToken,  //액세스토큰
    //        userId: success.authResponse.userID, //유저아이디
    //        date: new Date()  //날짜
    //      };
    //
    //      $localstorage.setObject('facebook', facebook);  //로컬스토리지에 저장
    //      AuthService.signIn(ENUMS.OAUTH.FACEBOOK, facebook.accessToken)
    //        .then(function (success){
    //          var userState = Session.get('state');
    //          $log.debug('signIn success', success);
    //          $log.debug('Session.get(state)', userState);
    //
    //          if (_.isEqual(userState, ENUMS.USER_STATE.PENDING)) {
    //            $log.debug('USER_STATE.PENDING');
    //            $state.go('app.registerInfo');
    //          }
    //          else if (_.isEqual(userState, ENUMS.USER_STATE.UNCERTIFIED)) {
    //            $log.debug('USER_STATE.UNCERTIFIED');
    //            $ionicHistory. clearHistory();
    //            $state.go('app.newsfeed', {feed: ENUMS.NEWSFEED.HOT});
    //          }
    //          else if (_.isEqual(userState, ENUMS.USER_STATE.ACTIVE)) {
    //            $log.debug('USER_STATE.ACTIVE');
    //            $ionicHistory. clearHistory();
    //            $state.go('app.newsfeed', {feed: ENUMS.NEWSFEED.HOT});
    //          }
    //          $rootScope.$broadcast('signIn', null);  //로그인함
    //        }, function (error){
    //          $log.debug('signIn error', error);
    //        });
    //
    //    }, function (error) {
    //      $log.debug('error', error);
    //    });
    //}

  }])

  .controller('RegisterInfoCtrl',
  ['$rootScope', '$scope', '$log', '$state', '$ionicHistory', '$cordovaDatePicker', '$cordovaPush', 'GCM', 'AuthService', 'Env', 'ENUMS',
    function($rootScope, $scope, $log, $state, $ionicHistory, $cordovaDatePicker, $cordovaPush, GCM, AuthService, Env, ENUMS) {
      $scope.user = {};
      $scope.apiURL = Env.API_URL;
      $scope.showDatePicker = function (){
        //document.addEventListener('deviceready', function () {
          var options = {
            date: new Date(),
            mode: 'date', // or 'time'
            allowOldDates: true,
            allowFutureDates: false,
            doneButtonLabel: '선택',
            doneButtonColor: '#F2F3F4',
            cancelButtonLabel: '취소',
            cancelButtonColor: '#000000'
          };
          $cordovaDatePicker.show(options).then(function(date){
            $scope.user.birth = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
          });

        //}, false);
      };


      //회원가입 완료
      $scope.signUp = function (user){
        AuthService.signUp(user).then(function (result){
          $ionicHistory.nextViewOptions({
            disableAnimate: true,
            disableBack: true,
            historyRoot: true
          });
          $rootScope.$broadcast('REGISTER_COMPLETED', null);  //가입 완료..레프트메뉴 갱신
          $ionicHistory. clearHistory();
          $state.go('app.newsfeed', {feed: ENUMS.NEWSFEED.HOT});
        });
      };
    }
  ]);