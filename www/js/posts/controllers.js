angular.module('linkat.posts.controllers', [])
  //메인 질문카드
  .controller('PostCtrl',
  ['$scope', '$log', '$state', '$mdDialog', 'AuthService', 'Posts',
    function($scope, $log, $state, $mdDialog, AuthService, Posts) {
      var $parentScope = $scope.$parent;

      //좋아요 토글
      $scope.togglelike = function (){
        $log.debug('togglelike', $scope.post);
        if(_.isNull($scope.post.liked)){
          $log.debug('togglelike', $scope.post.id, $scope.post.user_id);
          $scope.post.liked = true;
          $scope.post.like_count += 1;
          Posts.like($scope.post.id, $scope.post.user_id).then(function (success){
            $log.debug('success', success);
          }, function (error){
            $log.debug('error', error);
          });
        }else{
          $log.debug('togglelike', $scope.post.id);
          $scope.post.liked = null;
          $scope.post.like_count -= 1;
          Posts.unlike($scope.post.id, $scope.post.user_id).then(function (success){
            $log.debug('success', success);
          }, function (error){
            $log.debug('error', error);
          });
        }
      };

      //메뉴 모달
      $scope.menu = function (ev){
        $log.debug('$scope.post', $scope.post);
        $mdDialog.show({
          controller: MenuDialogCtrl,
          templateUrl: 'templates/list/menu-modal.html',
          targetEvent: ev,
          locals: {
            isMine: AuthService.isMine($scope.post.user_id),
            Post: $scope.post
          }
        });
      };
      function MenuDialogCtrl($scope, $mdDialog, isMine, Post){
        //취소
        $scope.isMine = isMine;

        $scope.cancel = function (ev){
          $mdDialog.cancel();
        };

        //수정
        $scope.update = function (ev){
          $log.debug('post update', Post);
          $mdDialog.hide();

          //게시물 수정 view로 이동
          var post = {
            questionId: Post.id,
            gender: Post.gender_id,
            category: Post.category_id,
            content: Post.content
          };
          $state.go('app.post-update', {post: post});
        };//update

        //삭제
        $scope.delete = function (ev){
          $mdDialog.hide();

          var confirm = $mdDialog.confirm()
            .content('게시물을 삭제하시겠어요?')
            .ok('삭제')
            .ariaLabel('confirm')
            .cancel('취소');
          //.targetEvent(ev);

          //질문 삭제 확인 다이얼로그
          $mdDialog.show(confirm).then(function() {
            var postId = Post.id;//글 아이디

            Posts.delete(postId).then(function (success){
              $log.debug('delete', success);
              //dom에서 질문 제거: parent 컨트롤러의 질문 배열 ng-repeat
              _.remove($parentScope.posts, function (arr){
                if(arr.id === postId)
                  return true;
                return false;
              });
            }, function (error){
              $log.debug('delete error', error);
            });
          }, function() {
            $log.debug('취소');
          });
        };//delete
      }//MenuDialogCtrl

    }
  ])

  .controller('PostNewCtrl',
  ['$scope', '$state', '$ionicLoading', '$ionicHistory', '$ionicPlatform', '$mdToast', '$cordovaCamera', '$log', 'Posts',
    function($scope, $state, $ionicLoading, $ionicHistory, $ionicPlatform, $mdToast, $cordovaCamera, $log, Posts) {
      $scope.post = {
        picture: ''
      };
      $scope.submitted = false;
      $scope.pictureSelected = false;

      $scope.showSimpleToast = function() {
        $mdToast.show(
          $mdToast.simple()
            .content('sdgsdgsdgdsgsdgds')
            .capsule(true)
            .position('top')
            .hideDelay(3000)
        );
      };

      $scope.gallery = function () {
        document.addEventListener("deviceready", function () {
          //갤러리에서 이미지 경로를 가져와서 다음 화면으로 넘김
          $cordovaCamera.getPicture({
            quality: 100,
            //destinationType: navigator.camera.DestinationType.FILE_URI,
            encodingType: Camera.EncodingType.JPEG,
            correctOrientation: true,
            //destinationType: Camera.DestinationType.DATA_URL,
            destinationType: Camera.DestinationType.FILE_URI,
            sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
            targetWidth: 1080,
            targetHeight: 1080
          }).then(function (imageURI) {
            //$scope.post.picture = 'data:image/jpeg;base64,' + imageURI;
            $log.debug('imageURI', imageURI);
            $scope.post.picture = imageURI;
            $scope.pictureSelected = true;
          }, function (message) {
            $log.debug('getPicture', message);
          });
        }, false);

      };//gallery

      $scope.submit = function () {
        $scope.submitted = true;
        $ionicLoading.show({
          //template: '<md-progress-circular md-mode="indeterminate"></md-progress-circular>'
          template: '<i class="ion-loading-c" style="color: #666; font-size: 30px"></i>'
        });

        //document.addEventListener("deviceready", function () {
          Posts.insert($scope.post).then(function (success){
            var questionId = JSON.parse(success).questionId;
            $ionicLoading.hide();
            $ionicHistory.currentView($ionicHistory.backView());
            $state.go('app.post', {postId: questionId}, {location: 'replace'});
          }, function (error){
            $log.debug('error', error);
          });
        //}, false);
        //Posts.insert($scope.post).then(function (success){
        //  var questionId = JSON.parse(success).questionId;
        //  $ionicLoading.hide();
        //  $ionicHistory.currentView($ionicHistory.backView());
        //  $state.go('app.post', {postId: questionId}, {location: 'replace'});
        //}, function (error){
        //  $log.debug('error', error);
        //});
      };
    }
  ])

  //질문 수정 컨트롤러
  .controller('PostUpdateCtrl',
  ['$scope', '$state', '$stateParams', '$ionicHistory', '$log', 'Posts',
    function($scope, $state, $stateParams, $ionicHistory, $log, Posts) {
      $scope.post = $stateParams.post;
      $log.debug($stateParams.post);

      $scope.next = function () {
        Posts.update($scope.post).then(function (success){
          $log.debug('update success', success);
          $ionicHistory.currentView($ionicHistory.backView());
          $state.go('app.post', {postId: post.questionId}, {location: 'replace'});
        },function (error){
          $log.debug('error', error);
        });
      };
    }
  ])

  .controller('PostDetailCtrl',
  ['$scope', '$log', '$state','$stateParams', '$ionicHistory', '$mdDialog', '$ionicLoading', 'Posts', 'AuthService',
    function($scope, $log, $state, $stateParams, $ionicHistory, $mdDialog, $ionicLoading, Posts, AuthService) {
      $scope.isLoading = true;

      $ionicLoading.show();
      Posts.select($stateParams.postId).then(function (result){
        $log.debug('result', result);
        $ionicLoading.hide();
        $scope.post = result.post;
        $scope.answers = result.answers;
        $scope.questionComments = result.questionComments;
        $scope.isLoading = false;
      }, function (error){
        $log.debug('에러', error);
      });

      $scope.goToMypage = function (){
        $state.go('app.mypage.qlikes', {userUrl: $scope.post.user_url});
      };

        //좋아요 토글
      $scope.togglelike = function (){
        $log.debug('togglelike', $scope.post);
        if(_.isNull($scope.post.liked)){
          $log.debug('togglelike', $scope.post.id, $scope.post.user_id);
          $scope.post.liked = true;
          $scope.post.like_count += 1;
          Posts.like($scope.post.id, $scope.post.user_id).then(function (success){
            $log.debug('success', success);
          }, function (error){
            $log.debug('error', error);
          });
        }else{
          $log.debug('togglelike', $scope.post.id);
          $scope.post.liked = null;
          $scope.post.like_count -= 1;
          Posts.unlike($scope.post.id, $scope.post.user_id).then(function (success){
            $log.debug('success', success);
          }, function (error){
            $log.debug('error', error);
          });
        }
      };


      $scope.menu = function (ev) {
        $mdDialog.show({
          controller: MenuDialogCtrl,
          templateUrl: 'templates/list/menu-modal.html',
          targetEvent: ev,
          locals: {
            isMine: AuthService.isMine($scope.post.user_id),
            Post: $scope.post
          }
        });
      };
      function MenuDialogCtrl($scope, $mdDialog, isMine, Post){
        //취소
        $scope.isMine = isMine;

        $scope.cancel = function (ev){
          $mdDialog.cancel();
        };

        //삭제
        $scope.delete = function (ev){
          $mdDialog.hide();

          var confirm = $mdDialog.confirm()
            .content('게시물을 삭제하시겠어요?')
            .ok('삭제')
            .cancel('취소');
          //.targetEvent(ev);

          //질문 삭제 확인 다이얼로그
          $mdDialog.show(confirm).then(function() {
            var postId = $scope.post.id;//글 아이디

            Posts.delete(postId).then(function(success) {
              $log.debug('deletePost', success);
              $ionicHistory.currentView($ionicHistory.backView());
              $state.go('app.list', {}, {location: 'replace', reload: true});
            }, function(error){
              $log.debug('deletePost', error);
            });
          }, function() {
            $log.debug('취소');
          });
        };//delete

        //수정
        $scope.update = function (ev){
          $log.debug('post update');
          $mdDialog.hide();

          //게시물 수정 view로 이동
          var post = {
            questionId: Post.id,
            gender: Post.gender_id,
            category: Post.category_id,
            content: Post.content
          };
          $state.go('app.post-update', {post: post});
        };//update

      }//MenuDialogCtrl


      $scope.comments = function (){
        //댓글 뷰로 이동
        $state.go('app.comment', {
          postId: $scope.post.id,  //글 아이디
          ownerId: $scope.post.user_id  //글 소유자 아이디
        });
      };

      $scope.answer = function(){
        $state.go('app.answer-new', {
          postId: $scope.post.id,  //글 아이디
          ownerId: $scope.post.user_id  //글 소유자 아이디
        });
      };
    }
  ]);