angular.module('linkat.common.services', [])
  .factory('$localstorage',
  ['$window',
    function($window) {

      return {
        set: function (key, value) {
          $window.localStorage[key] = value;
        },
        get: function (key, defaultValue) {
          return $window.localStorage[key] || defaultValue;
        },
        setObject: function (key, value) {
          $window.localStorage[key] = JSON.stringify(value);
        },
        getObject: function (key) {
          return JSON.parse($window.localStorage[key] || '{}');
        },
        remove: function (key) {
          return $window.localStorage.removeItem(key);
        },
        getAll: function () {
          return $window.localStorage;
        },
        removeAll: function () {
          for (var key in $window.localStorage) {
            $window.localStorage.removeItem(key);
          }
        }

      };
    }
  ])

  .factory('GcmService',
  ['$http', '$log', '$q', 'Session', 'Env',
    function($http, $log, $q, Session, Env) {
      return {
        insert: function (device) {
          var accessToken = Session.get('accessToken');
          //device = {uuid, regid}
          //uuid: 기기 아이디
          var config = {
            method: 'POST',
            url: Env.API_URL + '/device',
            data: device,
            headers: {
              'x-auth-token': accessToken
            }
          };
          $log.debug('GcmService config', config);
          var deferred = $q.defer();

          $http(config)
            .then(function (result, status, headers, config) {
              deferred.resolve(result.data);
            }, function (msg) {
              deferred.reject(msg);
            });
          return deferred.promise;
        }
      }
    }
  ]);