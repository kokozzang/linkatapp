// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js

angular.module('linkat',
    ['ionic',
        'ngCordova',
        'ngMaterial',
        'ngCordovaOauth',
        'wu.masonry',
        'angularMoment',
        'monospaced.elastic',
        'remoteValidation',
        'linkat.constants',
        'linkat.values',
        'linkat.directives',
        'linkat.common',
        'linkat.auth',
        'linkat.leftmenu',
        'linkat.settings',
        'linkat.notification',
        'linkat.answers',
        'linkat.comments',
        'linkat.mypage',
        'linkat.follow',
        'linkat.posts'])
    //'linkat.controllers', 'linkat.services', 'linkat.constants', 'linkat.directives'])

  .run(function($ionicPlatform, $rootScope, $log, $state, $localstorage, $cordovaDevice, $cordovaPush, $ionicHistory, Session, ENUMS, AUTH_EVENTS, GcmService, AuthService, amMoment, GCM, debugMode) {
    $ionicPlatform.ready(function() {
      $log.debug('angular.run');
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }



      $log.debug('screen.width', screen.width);
      $log.debug('window.devicePixelRatio', window.devicePixelRatio);
      $log.debug('screen.width * ratio', screen.width * window.devicePixelRatio);
      debugMode = ENUMS.MODE.DEBUG;

      amMoment.changeLocale('ko');

      var facebook = null;
      if(debugMode) {
        var tempLocalStorage = {
          accessToken:'CAAH2gpXsbj8BAHO7iixVmXQJXugh66dkX2642QMhCQdZCeJZAoUT8p1PMv2FYsIpsNUO3qD16BgdApDlk5OlHh23B1XcZBX2bNAGGAJLa98jnygyT9MRW8ofRkllf80nt8ILZAWZAtWfQRDlkPiIB2HLITZAqYudSovcc6h37lyUOtSZBEDqx5M',
          date: new Date()
        };
        facebook = !_.isEmpty($localstorage.getObject('facebook')) ?
          $localstorage.getObject('facebook') //기기인 경우
          : tempLocalStorage;
      }else {
        facebook = $localstorage.getObject('facebook'); //배포용
      }
      //facebook = $localstorage.getObject('facebook'); //배포용
      $log.debug('localstorage facebook', facebook);


      //facebook 첫 로그인이 아닌 경우
      if(!_.isEmpty(_.keys(facebook))){
        AuthService.verifyAccessToken(facebook.accessToken).
          then(function(success){
            $log.debug('verifyAccessToken success', success);
            AuthService.signIn(ENUMS.OAUTH.FACEBOOK, facebook.accessToken)
              .then(function (success){
                var userState = Session.get('state');
                $log.debug('signIn success', success);
                $log.debug('Session.get(state)', userState);

                if (_.isEqual(userState, ENUMS.USER_STATE.PENDING)) {
                  $log.debug('USER_STATE.PENDING');
                  $rootScope.$broadcast('EVT_REGISTER_GCM', null);  //로그인함
                  $state.go('app.registerInfo');
                }
                else if (_.isEqual(userState, ENUMS.USER_STATE.UNCERTIFIED)) {
                  $log.debug('USER_STATE.UNCERTIFIED');
                  //$state.go('app.registerInfo');
                  //$state.go('app.intro');
                  $rootScope.$broadcast('EVT_REGISTER_GCM', null);  //로그인함
                  $state.go('app.newsfeed', {feed: ENUMS.NEWSFEED.HOT});
                  //$state.go('app.mypage.qlikes' , {userUrl: 'qqq'});
                  //$state.go('app.answer-new');
                }
                else if (_.isEqual(userState, ENUMS.USER_STATE.ACTIVE)) {
                  $log.debug('USER_STATE.ACTIVE');
                  $rootScope.$broadcast('EVT_REGISTER_GCM', null);  //로그인함
                  $state.go('app.newsfeed', {feed: ENUMS.NEWSFEED.HOT});
                }else {
                  $state.go('app.intro');
                }
              }, function (error){
                $log.debug('signIn error', error);
              });
          }, function (error){
            $log.debug('verifyAccessToken error', error);
            $state.go('app.intro');
          });
      }else{  //facebook 첫 로그인인 경우
        $state.go('app.intro');
      }


      $rootScope.$on('EVT_REGISTER_GCM', function (event, isIntro) {
        $log.debug('EVT_REGISTER_GCM');
        //gcm 등록
        $cordovaPush.register({senderID: GCM.SENDER}).then(function (result) {
          $log.debug('$cordovaPush.register:', result);
        }, function (err) {
          $log.debug('$cordovaPush.register:', err);
        });
      });

      $rootScope.$on('$cordovaPush:notificationReceived', function(event, notification) {
        switch(notification.event) {
          case 'registered':  // 안드로이드 디바이스의 registerID를 획득하는 event 중 registerd 일 경우 호출된다.
            if (notification.regid.length > 0 ) {
              $log.debug('registerID', notification.regid);
              $log.debug('$cordovaDevice.getDevice()', $cordovaDevice.getDevice());
              $log.debug('$cordovaDevice.getUUID()', $cordovaDevice.getUUID());

              var device = {
                uuid: $cordovaDevice.getUUID(),
                regid: notification.regid
              };

              if(!$localstorage.get('gcmRegistered')){
                GcmService.insert(device).then(function(success){
                  $log.debug('GCM.insert success', success);
                  $localstorage.setObject('gcmRegistered', true);  //GCM registered 갱신
                }, function (err){
                  $log.debug('GCM.insert error', err);
                });
              }
            }
            break;

          case 'message': // 안드로이드 디바이스에 푸시 메세지가 오면 호출된다.
            $log.debug('notification', notification);
            $log.debug('message = ' + notification.message + ' msgCount = ' + notification.msgcnt);

            if (notification.foreground){ // 푸시 메세지가 왔을 때 앱이 실행되고 있을 경우
              $log.debug("푸시 메세지가 왔을 때 앱이 실행되고 있을 경우");
              //var soundfile = e.soundname || e.payload.sound;
              //var my_media = new Media("/android_asset/www/" + soundfile);
              //my_media.play();
            } else { // 푸시 메세지가 왔을 때 앱이 백그라운드로 실행되거나 실행되지 않을 경우
              if (notification.coldstart) { // 푸시 메세지가 왔을 때 푸시를 선택하여 앱이 열렸을 경우
                $log.debug("알림 왔을 때 앱이 열리고 난 다음에 실행 될때");
                var payload = notification.payload;

                switch(parseInt(payload.evt)){
                  case ENUMS.EVENT.COMMENT:
                    $log.debug('댓글 푸시');
                    //$state.go('app.intro');
                    //$state.go('app.comment', {postId: payload.postId, ownerId: payload.ownerId});
                    $state.go('app.comment', {postId: payload.postId, ownerId: payload.ownerId});
                    break;

                  case ENUMS.EVENT.ANSWER:
                    $log.debug('답변 푸시');
                    $state.go('app.post', {postId: payload.postId});
                    break;
                  default :
                    $log.debug('알수 없는 푸시');
                    break;
                }
              } else { // 푸시 메세지가 왔을 때 앱이 백그라운드로 사용되고 있을 경우
                $log.debug("앱이 백그라운드로 실행될 때");
              }
            }
            $log.debug('notification.payload.title', notification.payload.title);
            break;

          case 'error': // 푸시 메세지 처리에 에러가 발생하면 호출한다.
            $log.debug('GCM error = ', notification.msg);
            break;

          default:
            $log.debug('알수없는 이벤트');
            break;
        }
      });



  //    $rootScope.$on('$stateChangeStart', function (event, next) {
  //      if (!AuthService.isAuthenticated()) { //로그인 여부 확인부
  //        //console.log('gg');
  //        ////이동하려던 URL을 확인해서 페이지를 이동시킴
  //        //if(_.isEqual(next.url, '/signup')) {  //회원가입
  //        //  $rootScope.$broadcast('enterIntro', false);
  //        //  $location.path(next.url).replace();
  //        //}
  //        //else if(_.isEqual(next.url, '/registerInfo')) {
  //        //  $rootScope.$broadcast('enterIntro', false);
  //        //  $location.path(next.url).replace();
  //        //}
  //        //else {
  //        //  $rootScope.$broadcast('enterIntro', true);
  //        //  $log.info('intro로 전환');
  //        //  $location.path('/intro').replace();
  //        //}
  //      }
  ////      var authorizedRoles = next.data.authorizedRoles;
  ////      if (!AuthService.isAuthorized(authorizedRoles)) {
  ////        console.log('run if');
  ////        event.preventDefault();
  ////        if (AuthService.isAuthenticated()) {
  ////          // user is not allowed
  //////          $location.url('/someNewPath');
  ////          $location.path('/someNewPath');
  ////          $location.replace();
  //////          $rootScope.$broadcast(AUTH_EVENTS.notAuthorized);
  ////        } else {
  ////          // user is not logged in
  ////          $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated);
  ////        }
  ////      }
  //    });
  //    $state.go('app.intro', null, {location: 'replace'});  //뒤로가기...
  //    $state.go('app.post', {postId: 11}, {location: 'replace'});  //뒤로가기...
  //    $state.go('app.newsfeed' , {feed: ENUMS.NEWSFEED.HOT}, {location: 'replace'});  //뒤로가기...
  //    $state.go('app.mypage.qlikes' , {userUrl: '9bbing'});  //뒤로가기...
    });
  })


  .provider('Env', function ($logProvider, ENV){
    var config = null;

    function Env(){
      this.API_URL = null;
    }

    this.debugEnabled = function (isDebug){
      if(isDebug)
        config = ENV.DEBUG;
      else
        config = ENV.RELEASE;

      $logProvider.debugEnabled(config.LOG);  //디버그 로그 off
      Env.API_URL = config.API_URL; //URL 셋팅
    }
    this.$get = function (){
      return Env;
    };
  })

  .config(function($ionicConfigProvider, $stateProvider, $urlRouterProvider, EnvProvider) {
  // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $ionicConfigProvider.backButton.text('');
    $ionicConfigProvider.navBar.alignTitle('center');
    //  $httpProvider.defaults.withCredentials = true;

    EnvProvider.debugEnabled(true);

    $stateProvider
      // setup an abstract state for the tabs directive
      .state('app', {
        abstract: true,
        templateUrl: 'templates/left-menu.html',
        controller: 'AppCtrl'
      })

      .state('app.intro', {
        views: {
          'menuContent': {
            templateUrl: 'templates/intro.html',
            controller: 'IntroCtrl'
          }
        }
      })

      .state('app.registerInfo', {
        views: {
          'menuContent': {
            templateUrl: 'templates/auth/registerInfo.html',
            controller: 'RegisterInfoCtrl'
          }
        }
      })

      .state('app.newsfeed', {
        //resolve: {
        //  posts: function ($q, $log, Posts) { //질문들 가져옴
        //    var deferred = $q.defer();
        //    Posts.newsfeed().then(function (result) {
        //      $log.debug('result.questionResult', result.questionResult);
        //      deferred.resolve(result.questionResult);
        //    });
        //    return deferred.promise;
        //  }
        //},
        //cache: false,
        params: {
          feed: null
        },
        views: {
          'menuContent' :{
            templateUrl: 'templates/list/list.html',
            controller: 'NewsfeedCtrl'
          }
        }
      })

      .state('app.category', {
        params: {
          category: null
        },
        views: {
          'menuContent' :{
            templateUrl: 'templates/list/list.html',
            controller: 'CategoryCtrl'
          }
        }
      })

      .state('app.notification', {
        views: {
          'menuContent' :{
            templateUrl: 'templates/notification/notification.html',
            controller: 'NotificationListCtrl'
          }
        }
      })


      /*
       질문
       */
      .state('app.post', {  //질문 상세
        //resolve: {
        //  result: function ($q, $log, $stateParams, Posts) { //질문들 가져옴
        //    var deferred = $q.defer();
        //    Posts.select($stateParams.postId).then(function (result){
        //      $log.debug('result', result);
        //      deferred.resolve(result);
        //    }, function (error){
        //    $log.debug('에러', error);
        //    });
        //    return deferred.promise;
        //  }
        //},
        cache: false,
        params: {
          postId: null
        },
        views: {
          'menuContent' :{
            templateUrl: 'templates/post/post-detail.html',
            controller: 'PostDetailCtrl'
          }
        }
      })

      .state('app.post-new', {  //질문 하기
        //params: {
        //  post: null  //{picture} 기기상의 이미지의 uri 를 넘김
        //},
        cache: false,
        views: {
          'menuContent' :{
            templateUrl: 'templates/post/new/post-new.html',
            controller: 'PostNewCtrl'
          }
        }
      })

      .state('app.post-update', { //질문 수정
        params: {
          post: null
        },
        cache: false,
        views: {
          'menuContent' :{
            templateUrl: 'templates/post/post-update.html',
            controller: 'PostUpdateCtrl'
          }
        }
      })

      /*
       답변
       */
      .state('app.answer-new', {  //답변하기 - 사진 선택
        cache: false,
        params: {
          postId: null,
          ownerId: null
        },
        views: {
          'menuContent' :{
            templateUrl: 'templates/answer/new/answer-new.html',
            controller: 'AnswerNewCtrl'
          }
        }
      })

      .state('app.answer-update', { //답변 수정
        params: {
          post: null
        },
        cache: false,
        views: {
          'menuContent' :{
            templateUrl: 'templates/answer/answer-update.html',
            controller: 'AnswerUpdateCtrl'
          }
        }
      })

      /*
      댓글
       */
      .state('app.comment', { //댓글
        cache: false,
        params: {
          postId: null,
          ownerId: null
        },
        views: {
          'menuContent' :{
            templateUrl: 'templates/comment/comment.html',
            controller: 'CommentListCtrl'
          }
        }
      })

      /*
      마이페이지
       */
      .state('app.mypage', {
        cache: false,
        params: {
          userUrl: null
        },
        views: {
          'menuContent' :{
            templateUrl: 'templates/mypage/mypage.html',
            controller: 'MyPageCtrl'
          }
        }
      })

      .state('app.mypage.qlikes', {//나도궁금
        params: {
          userUrl: null
        },
        views: {
          'tab-content' :{
            templateUrl: 'templates/mypage/tab-qlikes.html',
            controller: 'MyPageTabQlikesCtrl'
          }
        }
      })

      .state('app.mypage.alikes', {//갖고싶어
        params: {
          userUrl: null
        },
        views: {
          'tab-content' :{
            templateUrl: 'templates/mypage/tab-alikes.html',
            controller: 'MyPageTabAlikesCtrl'
          }
        }
      })

      .state('app.mypage.questions', {//찾아줘요
        params: {
          userUrl: null
        },
        views: {
          'tab-content' :{
            templateUrl: 'templates/mypage/tab-questions.html',
            controller: 'MyPageTabQuestionsCtrl'
          }
        }
      })

      .state('app.mypage.answers', {//찾았어요
        params: {
          userUrl: null
        },
        views: {
          'tab-content' :{
            templateUrl: 'templates/mypage/tab-answers.html',
            controller: 'MyPageTabAnswersCtrl'
          }
        }
      })


      /*
       팔로우
       */
      .state('app.followers', {
        params: {
          userUrl: null
        },
        views: {
          'menuContent' :{
            templateUrl: 'templates/follow/follower.html',
            controller: 'followerListCtrl'
          }
        }
      })
      .state('app.followings', {
        params: {
          userUrl: null
        },
        views: {
          'menuContent' :{
            templateUrl: 'templates/follow/following.html',
            controller: 'followingListCtrl'
          }
        }
      })


      /*
      settings
       */
      .state('app.settings', {
        views: {
          'menuContent' :{
            templateUrl: 'templates/settings/settings.html',
            controller: 'SettingCtrl'
          }
        }
      })
      .state('app.settings-notification', {
        views: {
          'menuContent' :{
            templateUrl: 'templates/settings/notification/notification.html',
            controller: 'NotificationSettingCtrl'
          }
        }
      })

      .state('app.settings-profile', {
        cache: false,
        views: {
          'menuContent' :{
            templateUrl: 'templates/settings/profile/profile.html',
            controller: 'ProfileSettingCtrl'
          }
        }
      })
      .state('app.settings-profile-email', {
        params: {
          email: null
        },
        views: {
          'menuContent' :{
            templateUrl: 'templates/settings/profile/email.html',
            controller: 'ProfileSettingEmailCtrl'
          }
        }
      })
      .state('app.settings-profile-nickname', {
        params: {
          nickname: null
        },
        views: {
          'menuContent' :{
            templateUrl: 'templates/settings/profile/nickname.html',
            controller: 'ProfileSettingNicknameCtrl'
          }
        }
      })
      .state('app.settings-profile-userurl', {
        params: {
          userUrl: null
        },
        views: {
          'menuContent': {
            templateUrl: 'templates/settings/profile/userurl.html',
            controller: 'ProfileSettingUserURLCtrl'
          }
        }
      })
      .state('app.settings-profile-introduce', {
        params: {
          introduce: null
        },
        views: {
          'menuContent' : {
            templateUrl: 'templates/settings/profile/introduce.html',
            controller: 'ProfileSettingIntroduceCtrl'

          }
        }
      })
      .state('app.settings-agreement', {
            views: {
                'menuContent' :{
                    templateUrl: 'templates/settings/agreement.html'
                }
            }
        })
        .state('app.settings-privacy', {
            views: {
                'menuContent' :{
                    templateUrl: 'templates/settings/privacy.html'
                }
            }
        });



        // if none of the above states are matched, use this as the fallback
        //$urlRouterProvider
        //  .otherwise('/hot');

    });
