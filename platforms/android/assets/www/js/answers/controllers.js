//AnswerCtrl
function AnswerCtrl($scope, $log, $mdDialog, Answers, AuthService) {
  this.scope = $scope;
  this.log = $log;
  this.mdDialog = $mdDialog;
  this.isMine = AuthService.isMine($scope.answer.user_id);
  this.parentScope = $scope.$parent;
  this.Answers = Answers;
}

AnswerCtrl.prototype.toggleLike = function (){
  var $scope = this.scope;
  var $log = this.log;
  var Answers = this.Answers;

  $log.debug('$scope.answer.liked', $scope.answer.liked);
  if(_.isNull($scope.answer.liked)){
    $log.debug('togglelike', $scope.answer.id, $scope.answer.user_id);
    $scope.answer.liked = true;
    $scope.answer.like_count += 1;
    Answers.like($scope.answer.question_id, $scope.answer.id, $scope.answer.user_id).then(function (success){
      $log.debug('success', success);
    }, function (error){
      $log.debug('error', error);
    });
  }else{
    $log.debug('togglelike', $scope.answer.id);
    $scope.answer.liked = null;
    $scope.answer.like_count -= 1;
    Answers.unlike($scope.answer.question_id, $scope.answer.id, $scope.answer.user_id).then(function (success){
      $log.debug('success', success);
    }, function (error){
      $log.debug('error', error);
    });
  }
};

//AnswerCtrl.prototype.turnback = function (){
//  var $scope = this.scope;
//  var $log = this.log;
//
//
//};

AnswerCtrl.prototype.menu = function (ev){
  var $scope = this.scope;
  var $mdDialog = this.mdDialog;
  var $log = this.log;
  var $parentScope = this.parentScope;
  var Answers = this.Answers;

  $log.debug('$scope.answer', $scope.answer);

  $mdDialog.show({
    controller: MenuDialogCtrl,
    templateUrl: 'templates/list/menu-modal.html',
    targetEvent: ev,
    locals: { //MenuDialogCtrl에 넘겨줄 변수
      $parentScope: $parentScope,
      isMine: this.isMine,
      answerId: $scope.answer.id,
      postId: $scope.answer.question_id
    }
  });

  function MenuDialogCtrl($scope, $log, $mdDialog, Answers, $parentScope, isMine, postId, answerId){
    $scope.isMine = isMine;

    $scope.cancel = function (ev){
      $mdDialog.cancel();
    };

    $scope.update = function (ev) {
      $mdDialog.hide();
      //var post = {
      //  questionId: Post.id,
      //  gender: Post.gender_id,
      //  category: Post.category_id,
      //  content: Post.content
      //};
      //$state.go('app.answer-update', {post: post});
    };

    $scope.delete = function (ev){
      $mdDialog.hide();
      var confirm = $mdDialog.confirm()
        .content('답변을 삭제하시겠어요?')
        .ariaLabel('confirm')
        .ok('아니오')
        .cancel('예');

      //댓글 삭제 확인 다이얼로그
      $mdDialog.show(confirm).then(function() {
        $log.debug('delete answerId ', answerId);
        $log.debug('delete postId', postId);

        Answers.delete(postId, answerId).then(function (success){
          $log.debug('delete', success);
          //dom에서 답변 제거: parent 컨트롤러의 답변 배열 ng-repeat
          _.remove($parentScope.answers, function (arr){
            if(arr.id === answerId)
              return true;
            return false;
          });
        }, function (error){
          $log.debug('delete error', error);
        });
      }, function() {
        $log.debug('취소');
      });//show
    };//delete
  }//MenuDialogCtrl
};


angular.module('linkat.answers.controllers', [])
  .controller('AnswerCtrl',
  ['$scope', '$log', '$mdDialog', 'Answers', 'AuthService',
    AnswerCtrl//상단에 정의
  ])

  .controller('AnswerNewCtrl',
  ['$scope', '$state', '$ionicHistory', '$stateParams', '$ionicLoading', '$cordovaCamera', '$mdToast', '$log', 'Answers',
    function($scope, $state, $ionicHistory, $stateParams, $ionicLoading, $cordovaCamera, $mdToast, $log, Answers) {
      $scope.answer = {
        postId: $stateParams.postId,
        ownerId: $stateParams.ownerId,
        picture: ''
      };
      $scope.submitted = false;
      $scope.pictureSelected = false;



      $scope.toastMaker = function (){


        if(form.$error){
          var message = '가격을 확인해주세요.';
          $mdToast.show(
            $mdToast.simple()
              .content(message)
              .capsule(true)
              .position('top')
              .hideDelay(2000)
          );
        }
      };

      $scope.gallery = function () {
        document.addEventListener("deviceready", function () {
          //갤러리에서 이미지 경로를 가져와서 다음 화면으로 넘김
          $cordovaCamera.getPicture({
            quality: 75,
            //destinationType: navigator.camera.DestinationType.FILE_URI,
            encodingType: Camera.EncodingType.JPEG,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
            targetWidth: 400,
            targetHeight: 400
          }).then(function (imageURI) {
            $scope.answer.picture = 'data:image/jpeg;base64,' + imageURI;
            $scope.pictureSelected = true;
          }, function (message) {
            $log.debug(message);
          });
        }, false);
      };  //gallery

      $scope.submit = function () {
        $scope.submitted = true;
        $ionicLoading.show({
          template: '<md-progress-circular md-mode="indeterminate"></md-progress-circular>'
        });
        Answers.insert($scope.answer).then(function (success){
          $ionicLoading.hide();
          $ionicHistory.currentView($ionicHistory.backView());
          $state.go('app.post', {postId: $scope.answer.postId}, {location: 'replace'});
        }, function (error){
          $log.debug('error', error);
        });
      };
    }
  ])

  //질문 수정 컨트롤러
  .controller('AnswersUpdateCtrl',
  ['$scope', '$state', '$stateParams', '$ionicHistory', '$log', 'Posts',
    function($scope, $state, $stateParams, $ionicHistory, $log, Posts) {
      $scope.post = $stateParams.post;
      $log.debug($stateParams.post);

      $scope.next = function () {
        Posts.update($scope.post).then(function (success){
          $log.debug('update success', success);
          $ionicHistory.currentView($ionicHistory.backView());
          $state.go('app.post', {postId: post.questionId}, {location: 'replace'});
        },function (error){
          $log.debug('error', error);
        });
      };
    }
  ]);