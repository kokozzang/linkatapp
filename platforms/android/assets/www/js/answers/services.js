angular.module('linkat.answers.services', [])
  .factory('Answers',
  ['$http', '$log', '$q', '$timeout', '$cordovaFileTransfer', 'Session', 'Env',
    function($http, $log, $q, $timeout, $cordovaFileTransfer, Session, Env) {
      var accessToken = Session.get('accessToken');
      
      return {
        insert: function (answer) {
          //파일 및 post 파라미터 전송
          var options = {
            httpMethod: 'POST',
            headers: {
              'x-auth-token': accessToken
            },
            mimeType: 'image/jpeg',
            fileKey: 'imgs',  //파일 파라미터명 ex)req.body.imgs
            fileName: 'image-' + Date.now() + '.jpg', //전송될 파일명
            params: { //기타 파라미터
              price: answer.price,
              link: answer.link,
              content: answer.content,
              ownerId: answer.ownerId
            }
          };
          $log.debug('answer', answer);
          $log.debug('options', options);

          var deferred = $q.defer();
          var url = Env.API_URL + '/posts/' + answer.postId+ '/answers';
          $cordovaFileTransfer.upload(url, answer.picture, options)
            .then(function(result) {
              deferred.resolve(result.response);  //{questionId}
            }, function(err) {
              $log.debug('err', err);
              deferred.reject(err);
            }, function (progress) {
              //$timeout(function () {
              //  $scope.downloadProgress = (progress.loaded / progress.total) * 100;
              //})
            });
            return deferred.promise;
        },

        update: function(post) {
          var config = {
            method: 'PUT',
            url: Env.API_URL + '/posts'+ '/' + post.questionId,
            headers: {
              'x-auth-token': accessToken
            },
            data: {
              gender: post.gender,
              category: post.category,
              content: post.content
            }
          };

          var deferred = $q.defer();
          $http(config)
            .then(function(result, status, headers, config) {
              deferred.resolve(result.data);
            }, function(msg) {
              deferred.reject(msg);
            });
          return deferred.promise;
        },

        delete: function(postId, answerId) {
          var config = {
            method: 'DELETE',
            url: Env.API_URL + '/posts/' + postId+ '/answers/' + answerId,
            headers: {
              'x-auth-token': accessToken
            }
          };

          var deferred = $q.defer();
          $http(config)
            .then(function(result, status, headers, config) {
              deferred.resolve(result.data);
            }, function(msg) {
              deferred.reject(msg);
            });
          return deferred.promise;
        },

        like: function (postId, answerId, ownerId) {
          var config = {
            method: 'POST',
            url: Env.API_URL +  '/posts/' + postId + '/answers/' + answerId + '/like',
            headers: {
              'x-auth-token': accessToken
            },
            data: {
              ownerId: ownerId
            }
          };

          var deferred = $q.defer();
          $http(config)
            .then(function(result, status, headers, config) {
              deferred.resolve(result.data);
            }, function(msg) {
              deferred.reject(msg);
            });
          return deferred.promise;
        },//like

        unlike: function (postId, answerId) {
          var config = {
            method: 'DELETE',
            url: Env.API_URL +  '/posts/' + postId + '/answers/' + answerId + '/like',
            headers: {
              'x-auth-token': accessToken
            }
          };
          var deferred = $q.defer();
          $http(config)
            .then(function(result, status, headers, config) {
              deferred.resolve(result.data);
            }, function(msg) {
              deferred.reject(msg);
            });
          return deferred.promise;
        }//unlike
      }
    }
  ]);