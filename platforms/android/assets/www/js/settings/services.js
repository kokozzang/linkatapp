angular.module('linkat.settings.services', [])
  .factory('Profile',
  ['$http', '$log', '$q', '$cordovaFileTransfer', 'Session', 'Env',
    function($http, $log, $q, $cordovaFileTransfer, Session, Env) {
      var accessToken = Session.get('accessToken');

      return {
        all: function () {
          var config = {
            method: 'GET',
            url: Env.API_URL + '/account',
            headers: {
              'x-auth-token': accessToken
            }
          };
          var deferred = $q.defer();
          $http(config)
            .then(function (result, status, headers, config) {
              deferred.resolve(result.data);
            }, function (msg) {
              deferred.reject(msg);
            });
          return deferred.promise;
        },//all

        profilePhoto: {
          update: function (image) {
            var options = {
              httpMethod: 'PUT',
              headers: {
                'x-auth-token': accessToken

          },
              mimeType: 'image/jpeg',
              fileKey: 'img',  //파일 파라미터명 ex)req.body.img
              fileName: 'profile-' + Date.now() + '.jpg' //전송될 파일명
            };
            $log.debug('options', options);
            $log.debug('image', image);
            var deferred = $q.defer();

            $cordovaFileTransfer.upload(Env.API_URL + '/account/profilePhoto', image, options)
              .then(function(result) {
                Session.store('profileSmall', JSON.parse(result.response).profile_small_url);
                Session.store('profileMedium', JSON.parse(result.response).profile_medium_url);
                deferred.resolve(result.response);
              }, function(err) {
                deferred.reject(err);
              }, function (progress) {
                //$timeout(function () {
                //  $scope.downloadProgress = (progress.loaded / progress.total) * 100;
                //})
              });
            return deferred.promise;
          }
        },//profilePhoto

        email: {
          update: function (email) {
            var config = {
              method: 'PUT',
              url: Env.API_URL + '/account/email',
              headers: {
                'x-auth-token': accessToken
              },
              data: {
                email: email
              }
            };

            var deferred = $q.defer();
            $http(config)
              .then(function (result, status, headers, config) {
                Session.store('email', email);
                deferred.resolve(result.data);
              }, function (msg) {
                deferred.reject(msg);
              });
            return deferred.promise;
          }
        },//email

        nickname: {
          update: function (nickname) {
            var config = {
              method: 'PUT',
              url: Env.API_URL + '/account/nickname',
              headers: {
                'x-auth-token': accessToken
              },
              data: {
                nickname: nickname
              }
            };

            var deferred = $q.defer();
            $http(config)
              .then(function (result, status, headers, config) {
                Session.store('nickname', nickname);
                deferred.resolve(result.data);
              }, function (msg) {
                deferred.reject(msg);
              });
            return deferred.promise;
          }
        },//nickname

        userURL: {
          update: function (userUrl) {
            var config = {
              method: 'PUT',
              url: Env.API_URL + '/account/userurl',
              headers: {
                'x-auth-token': accessToken
              },
              data: {
                userUrl: userUrl
              }
            };

            var deferred = $q.defer();
            $http(config)
              .then(function (result, status, headers, config) {
                Session.store('userUrl', userUrl);
                deferred.resolve(result.data);
              }, function (msg) {
                deferred.reject(msg);
              });
            return deferred.promise;
          }
        },//userURL

        introduce: {
          update: function (introduce) {
            var config = {
              method: 'PUT',
              url: Env.API_URL + '/account/introduce',
              headers: {
                'x-auth-token': accessToken
              },
              data: {
                introduce: introduce
              }
            };

            var deferred = $q.defer();
            $http(config)
              .then(function (result, status, headers, config) {
                deferred.resolve(result.data);
              }, function (msg) {
                deferred.reject(msg);
              });
            return deferred.promise;
          }
        }, //introduce
        notification: {
          update: function () {
            var config = {
              method: 'PUT',
              url: Env.API_URL + '/account/notification',
              headers: {
                'x-auth-token': accessToken
              }
            };

            var deferred = $q.defer();
            $http(config)
              .then(function (result, status, headers, config) {
                deferred.resolve(result.data);
              }, function (msg) {
                deferred.reject(msg);
              });
            return deferred.promise;
          }
        }//notification
        
      };//return
    }
  ]);