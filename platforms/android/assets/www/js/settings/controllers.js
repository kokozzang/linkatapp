angular.module('linkat.settings.controllers', [])
  .controller('SettingCtrl', //설정 화면 컨트롤러
  ['$scope', '$rootScope', '$log', '$state', '$stateParams', '$ionicHistory', '$cordovaCamera', 'Profile', 'EVT_PROFILE_UPDATE',
    function($scope, $rootScope, $log, $state, $stateParams, $ionicHistory, $cordovaCamera, Profile, EVT_PROFILE_UPDATE) {
      //Profile service는 settings에 있음
      Profile.all().then(function (success){
        $log.debug('all', success);
        $scope.profile = success.account;
      }, function (error){
        $log.debug('에러', error);
      });

      $scope.profilePhoto = function (){
        $cordovaCamera.getPicture({
          quality: 100,
          allowEdit: true,
          encodingType: Camera.EncodingType.JPEG,
          //destinationType: Camera.DestinationType.DATA_URL,
          destinationType: Camera.DestinationType.FILE_URI,
          sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
          targetWidth: 1080,
          targetHeight: 1080
        }).then(function (imageURI) {
          //$scope.profile.profile_medium_url = 'data:image/jpeg;base64,' + imageURI;
          $scope.profile.profile_medium_url = imageURI;
          $log.debug('$scope.profile.profile_medium_url', $scope.profile.profile_medium_url);
          Profile.profilePhoto.update($scope.profile.profile_medium_url).then(function (success){
            $log.debug('success', success);
            $rootScope.$broadcast(EVT_PROFILE_UPDATE.PROFILE_PHOTO);
            $ionicHistory.goBack();
          }, function (error){
            $log.debug('error', error);
          });
        }, function (message) {
          $log.debug('getPicture', message);
        });
      };
      $scope.email = function (){
        $state.go('app.settings-profile-email', {email: $scope.profile.email});
      };
      $scope.nickname = function (){
        $state.go('app.settings-profile-nickname', {nickname: $scope.profile.nickname});
      };
      $scope.userUrl = function (){
        $state.go('app.settings-profile-userurl', {userUrl: $scope.profile.user_url});
      };
      $scope.introduce = function (){
        $state.go('app.settings-profile-introduce', {introduce: $scope.profile.introduce});
      };

      $scope.toggleNotification = function () {
        Profile.notification.update().then(function (success){
          $log.debug('success', success);
        }, function (error){
          $log.debug('error', error);
        });
      }
    }
  ])

  .controller('ProfileSettingEmailCtrl',
  ['$scope', '$log', '$state', '$stateParams', '$ionicHistory', 'Profile', 'Env',
    function ($scope, $log, $state, $stateParams, $ionicHistory, Profile, Env) {
      var user = {
        email: $stateParams.email
      };
      $scope.user = user;
      $scope.API_URL = Env.API_URL;

      $scope.submit = function () {
        Profile.email.update($scope.user.email).then(function (success){
          $log.debug('success', success);
          $ionicHistory.goBack();
        }, function (error){
          $log.debug('error', error);
        });
      }
    }
  ])

  .controller('ProfileSettingNicknameCtrl',
  ['$scope', '$rootScope', '$log', '$state', '$stateParams', '$ionicHistory', 'Profile', 'Env', 'EVT_PROFILE_UPDATE',
    function ($scope, $rootScope, $log, $state, $stateParams, $ionicHistory, Profile, Env, EVT_PROFILE_UPDATE) {
      var user = {
        nickname: $stateParams.nickname
      };
      $scope.user = user;
      $scope.API_URL = Env.API_URL;

      $scope.submit = function () {
        Profile.nickname.update($scope.user.nickname).then(function (success){
          $log.debug('success', success);
          $rootScope.$broadcast(EVT_PROFILE_UPDATE.NICKNAME);
          $ionicHistory.goBack();
        }, function (error){
          $log.debug('error', error);
        });
      }
    }
  ])

  .controller('ProfileSettingUserURLCtrl',
  ['$scope', '$log', '$state', '$stateParams', '$ionicHistory', 'Profile', 'Env',
    function ($scope, $log, $state, $stateParams, $ionicHistory, Profile, Env) {
      var user = {
        userUrl: $stateParams.userUrl
      };
      $scope.user = user;
      $scope.API_URL = Env.API_URL;

      $scope.submit = function () {
        Profile.userUrl.update($scope.user.userUrl).then(function (success){
          $log.debug('success', success);
          $ionicHistory.goBack();
        }, function (error){
          $log.debug('error', error);
        });
      }
    }
  ])

  .controller('ProfileSettingIntroduceCtrl',
  ['$scope', '$log', '$state', '$stateParams', '$ionicHistory', 'Profile', 'Env',
    function ($scope, $log, $state, $stateParams, $ionicHistory, Profile, Env) {
      var user = {
        introduce: $stateParams.introduce
      };
      $scope.user = user;
      $scope.API_URL = Env.API_URL;

      $scope.submit = function () {
        Profile.introduce.update($scope.user.introduce).then(function (success){
          $log.debug('success', success);
          $ionicHistory.goBack();
        }, function (error){
          $log.debug('error', error);
        });
      }
    }
  ]);