angular.module('linkat.auth.services', [])

  .service('Session', function () {
    this.create = function (sessionId, id, userUrl, profileSmall, profileMedium, nickname, role, state, accessToken) {
      this.id = sessionId;
      this.userId = id;
      this.userUrl = userUrl;
      this.profileSmall = profileSmall;
      this.profileMedium = profileMedium;
      this.nickname = nickname;
      this.role = role;
      this.state = state;
      this.accessToken = accessToken;
    };
    this.destroy = function () {
      this.id = null;
      this.userId = null;
      this.userUrl = null;
      this.profileSmall = null;
      this.profileMedium = null;
      this.nickname = null;
      this.role = null;
      this.state = null;
      this.accessToken = null;
    };
    this.store = function (key, val) {
      this[key] = val;
    };
    this.get = function (key) {
      return this[key];
    };

    return this;
  })

  .factory('AuthService',
  ['$http', '$q', '$log', 'Env', 'ENUMS', 'Session',
    function ($http, $q, $log, Env, ENUMS, Session) {
      var authService = {};

      authService.signIn = function (oauth, accessToken) {
        if (_.isEqual(oauth, ENUMS.OAUTH.FACEBOOK)) {
          var deferred = $q.defer();
          var config = {
            method: 'POST',
            url: Env.API_URL + '/auth/facebook',
            headers: {
              'x-auth-token': accessToken //액세스 토큰 헤더
            }
          };

          $http(config)
            .then(function (result, status, headers, config) {
              var userInfo = result.data;
              Session.create(
                userInfo.id,  //sessionId
                userInfo.id,  //userId
                userInfo.user_url,
                userInfo.profile_small_url,
                userInfo.profile_medium_url,
                userInfo.nickname,
                null, //role
                userInfo.state_id,
                accessToken);
              deferred.resolve(result.data);
            }, function (msg) {
              deferred.reject(msg);
            });
          return deferred.promise;
        } else if (_.isEqual(oauth, ENUMS.OAUTH.TWITTER)) {
          //트위터 로그인
        }
      };

      //회원가입
      authService.signUp = function (user) {
        var accessToken = Session.get('accessToken');
        var config = {
          method: 'POST',
          url: Env.API_URL + '/user/registerInfo',
          data: user,
          headers: {
            'x-auth-token': accessToken
          }
        };
        var deferred = $q.defer();

        $http(config)
          .then(function (result, status, headers, config) {
            $log.debug('/user/registerInfo', result.data);
            var userInfo = result.data;
            Session.create(
              userInfo.id,  //sessionId
              userInfo.id,  //userId
              userInfo.user_url,
              userInfo.profile_small_url,
              userInfo.profile_medium_url,
              userInfo.nickname,
              null, //role
              userInfo.state_id,
              accessToken);
            $log.debug('signup Session', Session);
            deferred.resolve(userInfo);
          }, function (msg) {
            deferred.reject(msg);
          });
        return deferred.promise;
      };

      authService.verifyAccessToken = function (accessToken) {
        var config = {
          method: 'GET',
          url: 'https://graph.facebook.com/me',
          params: {
            access_token: accessToken
          }
        };

        var deferred = $q.defer();
        $http(config)
          .then(function (result, status, headers, config) {
            deferred.resolve(result.data);
          }, function (msg) {
            deferred.reject(msg);
          });
        return deferred.promise;

      };

      authService.isAuthenticated = function () {
        $log.debug('service authService.isAuthenticated Session.userId', Session.userId);
        return !!Session.userId;
      };

      authService.isAuthorized = function (authorizedRoles) {
        if (!angular.isArray(authorizedRoles)) {
          authorizedRoles = [authorizedRoles];
        }
        return (authService.isAuthenticated() &&
        authorizedRoles.indexOf(Session.userRole) !== -1);
      };

      authService.isMine = function (userId) {
        return _.isEqual(Session.get('userId'), userId);
      };

      return authService;
    }
  ]);