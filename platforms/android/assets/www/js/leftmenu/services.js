angular.module('linkat.leftmenu.services', [])
  .factory('Newsfeed',
  ['$http', '$log', '$q', '$timeout', 'Session', 'Env', 'ENUMS',
    function($http, $log, $q, $timeout, Session, Env, ENUMS) {
      var accessToken = Session.get('accessToken');
      
      return {
        list: function (feed, startId, page){
          $log.debug('service list');
          var config = {
            method: 'GET',
            url: Env.API_URL + feed,
            headers: {
              'x-auth-token': accessToken
            },
            params: {
              startId: startId,
              page: page,
              limit: ENUMS.PAGE_LOAD_LIMIT
            }
          };

          var deferred = $q.defer();
          $http(config)
            .then(function(result, status, headers, config) {
              deferred.resolve(result.data);
            }, function(msg) {
              deferred.reject(msg);
            });
          return deferred.promise;
        }
      };
    }
  ])

  .factory('Categories',
  ['$http', '$log', '$q', '$timeout', 'Session', 'Env', 'ENUMS',
    function($http, $log, $q, $timeout, Session, Env, ENUMS) {
      var accessToken = Session.get('accessToken');

      return {
        list: function (category, startId, page){
          var config = {
            method: 'GET',
            url: Env.API_URL + '/categories' + category,
            headers: {
              'x-auth-token': accessToken
            },
            params: {
              startId: startId,
              page: page,
              limit: ENUMS.PAGE_LOAD_LIMIT
            }
          };

          var deferred = $q.defer();
          $http(config)
            .then(function(result, status, headers, config) {
              deferred.resolve(result.data);
            }, function(msg) {
              deferred.reject(msg);
            });
          return deferred.promise;
        }

      };
    }
  ]);