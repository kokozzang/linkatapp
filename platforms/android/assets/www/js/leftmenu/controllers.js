angular.module('linkat.leftmenu.controllers', [])
  .controller('AppCtrl',
  ['$scope', '$rootScope', '$state', '$log', '$ionicHistory', '$cordovaPush', 'USER_ROLES', 'AuthService', 'Session', 'GCM', 'ENUMS',
    function($scope, $rootScope, $state, $log, $ionicHistory, $cordovaPush, USER_ROLES, AuthService, Session, GCM, ENUMS) {
      $log.debug('AppCtrl');

      $scope.isSideMenuEnabled = function (){
        $log.debug('AuthService.isAuthenticated()', AuthService.isAuthenticated());
        return AuthService.isAuthenticated();
      };

      $scope.goBack = function () {
        $ionicHistory.goBack();
      };
      $scope.clearDoubleFiredEvent = function (){
        return false;
      };

      $scope.goToLink = function (url) {
        window.open(url, '_system');
      }

      $scope.currentUser = null;
      //$scope.userRoles = USER_ROLES;
      $scope.isAuthorized = AuthService.isAuthorized;

      $scope.leftmenu = {
        user: {
          userId: Session.get('userId'),
          userUrl: Session.get('userUrl'),
          nickname: Session.get('nickname'),
          profileSmall: Session.get('profileSmall'),
          profileMedium: Session.get('profileMedium')
        }
      };

      $scope.NEWSFEED =  {
        HOT: ENUMS.NEWSFEED.HOT,
        NEW: ENUMS.NEWSFEED.NEW
      };
      
      $scope.CATEGORY =  {
        HOT: ENUMS.CATEGORY.CLOTHES,
        SHOES: ENUMS.CATEGORY.SHOES,
        ETCS: ENUMS.CATEGORY.ETCS
      };

      $log.debug('AuthService.isAuthenticated', AuthService.isAuthenticated());
      if (!AuthService.isAuthenticated()) {
        $log.debug('로그인 안되있음');
        //$ionicHistory.nextViewOptions({
        //  disableBack: true
        //});
        //$state.go('app.intro', {}, {location: 'replace'});  //뒤로가기...
      }

      $rootScope.$on('REGISTER_COMPLETED', function (event) {
        $scope.leftmenu = {
          user: {
            userId: Session.get('userId'),
            userUrl: Session.get('userUrl'),
            nickname: Session.get('nickname'),
            profileSmall: Session.get('profileSmall'),
            profileMedium: Session.get('profileMedium')
          }
        };

        $scope.leftmenu.mypage = function (){
          $ionicHistory.nextViewOptions({
            disableAnimate: true,
            disableBack: false,
            historyRoot: false
          });
          $state.go('app.mypage.qlikes', {userUrl: $scope.leftmenu.user.userUrl});
        };

        $scope.leftmenu.newsfeed = function (feed){
          $ionicHistory.nextViewOptions({
            disableAnimate: true,
            disableBack: true,
            historyRoot: true
          });
          $state.go('app.newsfeed', {feed: feed});
        };
        $scope.leftmenu.category = function (category){
          $ionicHistory.nextViewOptions({
            disableAnimate: true,
            disableBack: true,
            historyRoot: true
          });
          $state.go('app.category', {category: category});
        };

        $scope.leftmenu.settings = function () {
          $ionicHistory.nextViewOptions({
            disableAnimate: true,
            disableBack: false,
            historyRoot: false
          });
          $state.go('app.settings');
        };
      });

      $rootScope.$on('PROFILE_PHOTO_UPDATED', function (event) {
        $scope.leftmenu.user.profileSmall = Session.get('profileSmall');
        $scope.leftmenu.user.profileMedium = Session.get('profileMedium');
      });

      $rootScope.$on('PROFILE_NICKNAME_UPDATED', function (event) {
        $scope.leftmenu.user.nickname = Session.get('nickname');
      });


      $rootScope.$on('enterIntro', function (event, isIntro) {
        $scope.isIntro = isIntro;
      });

      $scope.leftmenu.mypage = function (){
        $ionicHistory.nextViewOptions({
          disableAnimate: true,
          disableBack: false,
          historyRoot: false
        });
        $state.go('app.mypage.qlikes', {userUrl: $scope.leftmenu.user.userUrl});
      };

      $scope.leftmenu.newsfeed = function (feed){
        $ionicHistory.nextViewOptions({
          disableAnimate: true,
          disableBack: true,
          historyRoot: true
        });
        $state.go('app.newsfeed', {feed: feed});
      };
      $scope.leftmenu.category = function (category){
        $ionicHistory.nextViewOptions({
          disableAnimate: true,
          disableBack: true,
          historyRoot: true
        });
        $state.go('app.category', {category: category});
      };

      $scope.leftmenu.settings = function () {
        $ionicHistory.nextViewOptions({
          disableAnimate: true,
          disableBack: false,
          historyRoot: false
        });
        $state.go('app.settings');
      };

    }
  ])

  .controller('NewsfeedCtrl',
  ['$scope', '$state', '$stateParams', '$log', '$mdDialog', '$ionicHistory', 'Newsfeed', 'ENUMS',
    //'posts',  //resolve
    function($scope, $state, $stateParams, $log, $mdDialog, $ionicHistory, Newsfeed, ENUMS) {
      var feed = null;  //feed 종류
      var startId = null; //첫번째 postId
      var page = 1; //현재 페이지

      if(_.isEqual($stateParams.feed, ENUMS.NEWSFEED.HOT)){
        feed = '/hot';
        $scope.title = 'HOT';
      } else if (_.isEqual($stateParams.feed, ENUMS.NEWSFEED.NEW)) {
        feed = '/new';
        $scope.title = 'NEW';
      } else {
        feed = '/hot';
        $scope.title = 'HOT';
      }

      Newsfeed.list(feed, startId, page).then(function (success) {
        $log.debug('success.posts', success.posts);
        $scope.posts = success.posts;
        $scope.moreDataCanBeLoaded = true;
        startId = $scope.posts.length ? $scope.posts[0].id: null;

        //로드한 post 갯수가 limit보다 작은 경우
        if(success.posts.length < ENUMS.PAGE_LOAD_LIMIT)
          $scope.moreDataCanBeLoaded = false;
        page++;
      }, function (error){
        $log.debug('error', error);
      });

      //무한스크롤
      $scope.loadPosts = function (){
        Newsfeed.list(feed, startId, page).then(function (success) {
          $log.debug('success.posts', success.posts);
          $scope.posts = $scope.posts.concat(success.posts);

          //로드한 post 갯수가 limit보다 작은 경우
          if(success.posts.length < ENUMS.PAGE_LOAD_LIMIT)
            $scope.moreDataCanBeLoaded = false;
          page++;
          $scope.$broadcast('scroll.infiniteScrollComplete');
        }, function (error){
          $log.debug('error', error);
        });
      };

      //refresh
      $scope.refresh = function (){
        startId = null;
        page = 1;

        Newsfeed.list(feed, startId, page).then(function (success) {
          $log.debug('success.posts', success.posts);
          $scope.posts = success.posts;
          $scope.moreDataCanBeLoaded = true;

          //로드한 post 갯수가 limit보다 작은 경우
          if(success.posts.length < ENUMS.PAGE_LOAD_LIMIT)
            $scope.moreDataCanBeLoaded = false;
          page++;
          $scope.$broadcast('scroll.refreshComplete');
        }, function (error){
          $log.debug('error', error);
        });
      };


      $scope.notifications = function (){
        $state.go('app.notification');
      };
    }
  ])

  .controller('CategoryCtrl',
  ['$scope', '$state', '$stateParams', '$log', '$mdDialog', '$ionicHistory', 'Categories', 'ENUMS',
    //'posts',  //resolve
    function($scope, $state, $stateParams, $log, $mdDialog, $ionicHistory, Categories, ENUMS) {
      var category = null;
      var startId = null; //첫번째 postId
      var page = 1; //현재 페이지

      if(_.isEqual($stateParams.category, ENUMS.CATEGORY.CLOTHES)) {
        category = '/clothes';
        $scope.title = '의류';
      } else if(_.isEqual($stateParams.category, ENUMS.CATEGORY.SHOES)) {
        category = '/shoes';
        $scope.title = '신발';
      } else if(_.isEqual($stateParams.category, ENUMS.CATEGORY.ETCS)) {
        category = '/etcs';
        $scope.title = '잡화';
      } else {
        category = '/clothes';
        $scope.title = '옷';
      }

      $scope.notifications = function (){
        $state.go('app.notification');
      };

      Categories.list(category, startId, page).then(function (success) {
        $log.debug('success.posts', success.posts);
        $scope.posts = success.posts;
        $scope.moreDataCanBeLoaded = true;  //무한스크롤 가능여부
        startId = $scope.posts.length ? $scope.posts[0].id: null;

        //로드한 post 갯수가 limit보다 작은 경우
        if(success.posts.length < ENUMS.PAGE_LOAD_LIMIT)
          $scope.moreDataCanBeLoaded = false;
        page++;
      }, function (error){
        $log.debug('error', error);
      });

      //무한스크롤
      $scope.loadPosts = function (){
        Categories.list(category, startId, page).then(function (success) {
          $log.debug('success.posts', success.posts);
          $scope.posts = $scope.posts.concat(success.posts);

          //로드한 post 갯수가 limit보다 작은 경우
          if(success.posts.length < ENUMS.PAGE_LOAD_LIMIT)
            $scope.moreDataCanBeLoaded = false;
          page++;
          $scope.$broadcast('scroll.infiniteScrollComplete');
        }, function (error){
          $log.debug('error', error);
        });
      };

      //refresh
      $scope.refresh = function (){
        startId = null;
        page = 1;

        Categories.list(category, startId, page).then(function (success) {
          $log.debug('success.posts', success.posts);
          $scope.posts = success.posts;
          $scope.moreDataCanBeLoaded = true;

          //로드한 post 갯수가 limit보다 작은 경우
          if(success.posts.length < ENUMS.PAGE_LOAD_LIMIT)
            $scope.moreDataCanBeLoaded = false;
          page++;
          $scope.$broadcast('scroll.refreshComplete');
        }, function (error){
          $log.debug('error', error);
        });
      };

    }
  ])