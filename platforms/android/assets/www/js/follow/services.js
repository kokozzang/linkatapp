angular.module('linkat.follow.services', [])
  .factory('Follow',
  ['$http', '$log', '$q', 'Session', 'Env',
    function($http, $log, $q, Session, Env) {
      var accessToken = Session.get('accessToken');

      return {
        followerList: function (userUrl) {
          var config = {
            method: 'GET',
            url: Env.API_URL + '/users/' + userUrl + '/followers',
            headers: {
              'x-auth-token': accessToken
            }
          };
          var deferred = $q.defer();
          $http(config)
            .then(function (result, status, headers, config) {
              deferred.resolve(result.data);
            }, function (msg) {
              deferred.reject(msg);
            });
          return deferred.promise;
        },
        followingList: function (userUrl) {
          var config = {
            method: 'GET',
            url: Env.API_URL + '/users/' + userUrl + '/followings',
            headers: {
              'x-auth-token': accessToken
            }
          };
          var deferred = $q.defer();
          $http(config)
            .then(function (result, status, headers, config) {
              deferred.resolve(result.data);
            }, function (msg) {
              deferred.reject(msg);
            });
          return deferred.promise;
        },
        insert: function (targetId) {
          var config = {
            method: 'POST',
            url: Env.API_URL + '/follow',
            headers: {
              'x-auth-token': accessToken
            },
            data: {
              targetId: targetId
            }
          };

          var deferred = $q.defer();
          $http(config)
            .then(function (result, status, headers, config) {
              deferred.resolve(result.data);
            }, function (msg) {
              deferred.reject(msg);
            });
          return deferred.promise;
        },
        delete: function (targetId) {
          $log.debug('targetId', targetId);
          var config = {
            method: 'DELETE',
            url: Env.API_URL + '/follow/' + targetId,
            headers: {
              'x-auth-token': accessToken
            }
          };

          var deferred = $q.defer();
          $http(config)
            .then(function (result, status, headers, config) {
              deferred.resolve(result.data);
            }, function (msg) {
              deferred.reject(msg);
            });
          return deferred.promise;
        }
      };//return
    }
  ]);