angular.module('linkat.mypage.services', [])
  .factory('Mypage',
  ['$http', '$log', '$q', 'Session', 'Env', 'ENUMS',
    function($http, $log, $q, Session, Env, ENUMS) {
      var accessToken = Session.get('accessToken');

      return {
        profile: function(userUrl) {
          var config = {
            method: 'GET',
            url: Env.API_URL + '/users/' + userUrl,
            headers: {
              'x-auth-token': accessToken
            }
          };

          var deferred = $q.defer();
          $http(config)
            .then(function(result, status, headers, config) {
              deferred.resolve(result.data);
            }, function(msg) {
              deferred.reject(msg);
            });
          return deferred.promise;
        },  //profiles

        qlikes: function(userUrl, startId, page) {
          var config = {
            method: 'GET',
            url: Env.API_URL + '/users/' + userUrl + '/qlikes',
            headers: {
              'x-auth-token': accessToken
            },
            params: {
              startId: startId,
              page: page,
              limit: ENUMS.PAGE_LOAD_LIMIT
            }
          };
          var deferred = $q.defer();
          $http(config)
            .then(function(result, status, headers, config) {
              deferred.resolve(result.data);
            }, function(msg) {
              deferred.reject(msg);
            });
          return deferred.promise;
        },//나도궁금

        alikes: function(userUrl, startId, page) {
          var config = {
            method: 'GET',
            url: Env.API_URL + '/users/' + userUrl + '/alikes',
            headers: {
              'x-auth-token': accessToken
            },
            params: {
              startId: startId,
              page: page,
              limit: ENUMS.PAGE_LOAD_LIMIT
            }
          };
          var deferred = $q.defer();
          $http(config)
            .then(function(result, status, headers, config) {
              deferred.resolve(result.data);
            }, function(msg) {
              deferred.reject(msg);
            });
          return deferred.promise;
        },//갖고싶어

        questions: function(userUrl, startId, page) {
          var config = {
            method: 'GET',
            url: Env.API_URL + '/users/' + userUrl + '/questions',
            headers: {
              'x-auth-token': accessToken
            },
            params: {
              startId: startId,
              page: page,
              limit: ENUMS.PAGE_LOAD_LIMIT
            }
          };
          var deferred = $q.defer();
          $http(config)
            .then(function(result, status, headers, config) {
              deferred.resolve(result.data);
            }, function(msg) {
              deferred.reject(msg);
            });
          return deferred.promise;
        },  //찾아줘요

        answers: function(userUrl, startId, page) {
          var config = {
            method: 'GET',
            url: Env.API_URL + '/users/' + userUrl + '/answers',
            headers: {
              'x-auth-token': accessToken
            },
            params: {
              startId: startId,
              page: page,
              limit: ENUMS.PAGE_LOAD_LIMIT
            }
          };
          var deferred = $q.defer();
          $http(config)
            .then(function(result, status, headers, config) {
              deferred.resolve(result.data);
            }, function(msg) {
              deferred.reject(msg);
            });
          return deferred.promise;
        }  //찾았어요

      };//return
    }//function
  ]);
