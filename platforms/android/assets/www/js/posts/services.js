angular.module('linkat.posts.services', [])
  .factory('Posts',
  ['$http', '$log', '$q', '$timeout', '$cordovaFileTransfer', '$ionicPlatform', 'Session', 'Env',
    function($http, $log, $q, $timeout, $cordovaFileTransfer, $ionicPlatform, Session, Env) {
      var accessToken = Session.get('accessToken');
      
      return {
        insert: function (post) {

          //파일 및 post 파라미터 전송
          var options = {
            httpMethod: 'POST',
            headers: {
              'x-auth-token': accessToken
            },
            mimeType: 'image/jpeg',
            fileKey: 'imgs',  //파일 파라미터명 ex)req.body.imgs
            fileName: 'image-' + Date.now() + '.jpg', //전송될 파일명
            params: { //기타 파라미터
              gender: post.gender,
              category: post.category,
              content: post.content
            }
          };
          $log.debug('post', post);
          $log.debug('options', options);
          var deferred = $q.defer();
          //document.addEventListener('deviceready', function () {
          //$ionicPlatform.ready(function () {


            $cordovaFileTransfer.upload(Env.API_URL + '/posts', post.picture, options)
              .then(function(result) {
                $log.debug('insertPost service', result);
                deferred.resolve(result.response);  //{questionId}
              }, function(err) {
                $log.debug('err', err);
                deferred.reject(err);
              }, function (progress) {
                $log.debug('progress', progress);
                //$timeout(function () {
                //  $scope.downloadProgress = (progress.loaded / progress.total) * 100;
                //})
              });
            return deferred.promise;
          //});
          //}, false);
        },

        select: function(postId) {
          var config = {
            method: 'GET',
            url: Env.API_URL + '/posts/' + postId,
            headers: {
              'x-auth-token': accessToken
            }
          };

          var deferred = $q.defer();
          $http(config)
            .then(function(result, status, headers, config) {
              deferred.resolve(result.data);
            }, function(msg) {
              deferred.reject(msg);
            });
          return deferred.promise;
        },

        update: function(post) {
          var config = {
            method: 'PUT',
            url: Env.API_URL + '/posts'+ '/' + post.questionId,
            headers: {
              'x-auth-token': accessToken
            },
            data: {
              gender: post.gender,
              category: post.category,
              content: post.content
            }
          };

          var deferred = $q.defer();
          $http(config)
            .then(function(result, status, headers, config) {
              deferred.resolve(result.data);
            }, function(msg) {
              deferred.reject(msg);
            });
          return deferred.promise;
        },

        delete: function(postId) {
          var config = {
            method: 'DELETE',
            url: Env.API_URL + '/posts'+ '/' + postId,
            headers: {
              'x-auth-token': accessToken
            }
          };

          var deferred = $q.defer();
          $http(config)
            .then(function(result, status, headers, config) {
              deferred.resolve(result.data);
            }, function(msg) {
              deferred.reject(msg);
            });
          return deferred.promise;
        },

        like: function (postId, ownerId) { //좋아요요요요용요
          var config = {
            method: 'POST',
            url: Env.API_URL +  '/posts/' + postId + '/like',
            headers: {
              'x-auth-token': accessToken
            },
            data: {
              ownerId: ownerId
            }
          };

          var deferred = $q.defer();
          $http(config)
            .then(function(result, status, headers, config) {
              deferred.resolve(result.data);
            }, function(msg) {
              deferred.reject(msg);
            });
          return deferred.promise;
        },//like

        unlike: function (postId) { //좋아요요요요용요
          var config = {
            method: 'DELETE',
            url: Env.API_URL +  '/posts/' + postId + '/like',
            headers: {
              'x-auth-token': accessToken
            }
          };
          var deferred = $q.defer();
          $http(config)
            .then(function(result, status, headers, config) {
              deferred.resolve(result.data);
            }, function(msg) {
              deferred.reject(msg);
            });
          return deferred.promise;
        }//unlike

      };
    }
  ]);